﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication3.ViewModel
{
    public class BMIData
    {
        internal string level;

        public float Weight { get; set; }
        public float Height { get; set; }
        public float BMI { get; set; }
        public string Level { get; set; }

    }
}